//Create a new board, create 3 lists simultaneously, and a card in each list simultaneously

APIkey = "21ae987b7a73ece5b631ea712594c3ba";
token =
  "ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444";

function createBoard(boardName) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=${APIkey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(`There is some error: ${error}`));
}

function createList(boardId, listName) {
  return fetch(
    `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIkey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(`There is some error: ${error}`));
}

function createCard(listId, cardName) {
  return fetch(
    `https://api.trello.com/1/cards?name=${cardName}&idList=${listId}&key=${APIkey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(`There is some error: ${error}`));
}
function createListsBoardsAndCards(boardName) {
  createBoard(boardName)
    .then((boardData) => {
      console.log(boardData.id);
      return boardData.id;
    })
    .then((idBoard) => {
      const listNames = ["Clothing", "Grocery", "Skincare"];
      let listPromises = listNames.map((name) => {
        return createList(idBoard, name);
      });
      return Promise.all(listPromises);
    })
    .then((arrayOfResolvedList) => {
      const idOfAllLists = arrayOfResolvedList.map((value) => value.id);
      const cardNames = ["T shirts", "Apple", "Sunscreen"];
      const cardPromises = cardNames.map((cardName, index) => {
        return createCard(idOfAllLists[index], cardName);
      });
      return Promise.all(cardPromises);
    })
    .then((value) => console.log(value))
    .catch((error) => console.log(error));
}

createListsBoardsAndCards("ShoppingShop");
