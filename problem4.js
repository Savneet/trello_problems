//Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data

function getCards(listId) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/cards?key=21ae987b7a73ece5b631ea712594c3ba&token=ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444`,
    {
      method: "GET",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(`There is some error: ${error}`));
}

getCards("6630867bf219faaa23d704e8") //Took the id of 1 of the list from 3rd problem
  .then((text) => {
    console.log(text);
  })
  .catch((err) => console.error(err));

module.exports = getCards;
