//Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data

function createBoard(boardName) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=21ae987b7a73ece5b631ea712594c3ba&token=ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(`There is some error: ${error}`));
}

createBoard("NodeJS")
  .then((data) => console.log(JSON.stringify(data)))
  .catch((err) => console.error(err));
