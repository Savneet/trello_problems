const getCardsOfList = require("./problem4.js");

// Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists.
// Do note that the cards should be fetched simultaneously from all the lists.

function getAllCards(boardId) {
  return fetch(
    `https://api.trello.com/1/boards/${boardId}/lists?key=21ae987b7a73ece5b631ea712594c3ba&token=ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444`
  )
    .then((response) => {
      return response.json();
    })
    .then((listsData) => {
      const cardPromise = listsData.map((list) => {
        return getCardsOfList(list.id);
      });
      return Promise.all(cardPromise);
    })
    .then((cardResolvedValueArray) => {
      return cardResolvedValueArray;
    })
    .catch((error) => console.log(error));
}

getAllCards("6630867bf219faaa23d704e1")
  .then((data) => console.log(data))
  .catch((error) => console.log(error));

module.exports = getAllCards;
