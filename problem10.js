const getAllCards = require("./problem5.js");

//Update all checkitems in a checklist to incomplete status sequentially i.e. Item 1 should be updated -> then wait for 1 second -> then Item 2 should be updated etc.

APIkey = "21ae987b7a73ece5b631ea712594c3ba";
token =
  "ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444";
function getCheckItems(checkListId) {
  return fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${APIkey}&token=${token}`,
    {
      method: "GET",
    }
  )
    .then((response) => {
      return response.json();
    })
    .catch((error) => {
      console.log(error);
    });
}

function incompleteStateOfCheckItem(cardId, checkItemId) {
  return fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=incomplete&key=${APIkey}&token=${token}`,
    {
      method: "PUT",
    }
  )
    .then((response) => {
      return response.text();
    })
    .catch((error) => {
      console.log(error);
    });
}

let cardChecklists = {};

getAllCards("6630867bf219faaa23d704e1")
  .then((cardsData) => {
    const flatCards = cardsData.flat();
    const promiseArrayOfgetCheckItems = [];
    flatCards.forEach((card) => {
      //console.log(card);
      cardChecklists[card.id] = card.idChecklists;
      console.log("cardChecklists");
      console.log(cardChecklists);
      if (card.idChecklists) {
        card.idChecklists.forEach((listId) => {
          promiseArrayOfgetCheckItems.push(getCheckItems(listId));
        });
      }
    });
    return Promise.all(promiseArrayOfgetCheckItems);
  })
  .then((checkItemsData) => {
    console.log(checkItemsData);
    const flatCheckItems = checkItemsData.flat();
    let cardId = Promise.resolve();
    flatCheckItems.forEach((checkItem) => {
      cardId = cardId.then(() => {
        Object.keys(cardChecklists).find((id) =>
          cardChecklists[id].includes(checkItem.idChecklist)
        );
        if (cardId) {
          return incompleteStateOfCheckItem(cardId, checkItem.id);
        }
      });
    });
  })
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });
