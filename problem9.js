const getAllCards = require("./problem5.js");
//Update all checkitems in a checklist to completed status simultaneously
APIkey = "21ae987b7a73ece5b631ea712594c3ba";
token =
  "ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444";
function getCheckItems(checkListId) {
  return fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${APIkey}&token=${token}`,
    {
      method: "GET",
    }
  )
    .then((response) => {
      return response.json();
    })
    .catch((error) => {
      console.log(error);
    });
}

function completeStateOfCheckItem(cardId, checkItemId) {
  fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=complete&key=${APIkey}&token=${token}`,
    {
      method: "PUT",
    }
  )
    .then((response) => {
      return response.json();
    })
    .catch((error) => {
      console.log(error);
    });
}

let cardChecklists = {};

getAllCards("6630867bf219faaa23d704e1")
  .then((cardsData) => {
    const flatCards = cardsData.flat();
    const promiseArrayOfgetCheckItems = [];
    flatCards.forEach((card) => {
      //console.log(card);
      cardChecklists[card.id] = card.idChecklists;
      console.log("cardChecklists");
      console.log(cardChecklists);
      if (card.idChecklists) {
        card.idChecklists.forEach((listId) => {
          promiseArrayOfgetCheckItems.push(getCheckItems(listId));
        });
      }
    });
    return Promise.all(promiseArrayOfgetCheckItems);
  })
  .then((checkItemsData) => {
    console.log(checkItemsData);
    const flatCheckItems = checkItemsData.flat();
    const promiseArray = [];
    flatCheckItems.forEach((checkItem) => {
      const cardId = Object.keys(cardChecklists).find((id) => {
        return cardChecklists[id].includes(checkItem.idChecklist);
      });
      if (cardId) {
        promiseArray.push(completeStateOfCheckItem(cardId, checkItem.id));
      }
    });
    return Promise.all(promiseArray);
  })
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });
