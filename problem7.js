//Delete all the lists created in Step 6 simultaneously

APIkey = "21ae987b7a73ece5b631ea712594c3ba";
token =
  "ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444";

function deleteList(listId) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/closed?key=${APIkey}&token=${token}&value=true`,
    {
      method: "PUT",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(error));
}

function createBoard(boardName) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=${APIkey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(`There is some error: ${error}`));
}

function createList(boardId, listName) {
  return fetch(
    `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIkey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(`There is some error: ${error}`));
}

function createCard(listId, cardName) {
  return fetch(
    `https://api.trello.com/1/cards?name=${cardName}&idList=${listId}&key=${APIkey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .catch((error) => console.log(`There is some error: ${error}`));
}

function createListsBoardsAndCards(boardName) {
  createBoard(boardName)
    .then((boardData) => {
      console.log(boardData.id);
      return boardData.id;
    })
    .then((idBoard) => {
      const listNames = ["Clothing", "Grocery", "Skincare"];
      const listPromises = [];
      listNames.forEach((name) => {
        listPromises.push(createList(idBoard, name));
      });
      return Promise.all(listPromises);
    })
    .then((arrayOfResolvedList) => {
      const idOfAllLists = arrayOfResolvedList.map((value) => value.id);
      const cardNames = ["T shirts", "Apple", "Sunscreen"];
      const cardPromises = [];
      for (let index = 0; index < 3; index++) {
        cardPromises.push(createCard(idOfAllLists[index], cardNames[index]));
      }
      return Promise.all(cardPromises).then(() => idOfAllLists);
    })
    .then((idOfAllLists) => {
      console.log("Attempting to delete lists:", idOfAllLists);
      let deleteListPromises = [];
      console.log(deleteListPromises);
      deleteListPromises = idOfAllLists.map((listId) => {
        return deleteList(listId);
      });
      return Promise.all(deleteListPromises);
    })
    .then((responses) => {
      console.log("Delete responses:", responses);
    })
    .then((value) => console.log(value))
    .catch((error) => console.log(error));
}

createListsBoardsAndCards("ShoppingShop");
